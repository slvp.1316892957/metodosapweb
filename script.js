// Transforme la hora actual en segundos.
function horaEnSegundos(){
    var hora = new Date();
    var segundosTotal= ((hora.getHours()*3600)+(hora.getMinutes()*60)+hora.getSeconds());
    alert("La hora actual es: " + hora + " y transformada en segundos es: " + segundosTotal);
}

// Calcular el área de un triángulo, que es igual a: (base * altura)/2. 
function areaTriangulo(){
    var base = parseInt(prompt("Ingrese la base del triangulo"));
    var altura = parseInt(prompt("Ingrese la altura del triangulo"));
    var area = (base*altura)/2
    alert("El resultado al calcular el area del triangulo es: " + area );
}

// Calcule la raíz cuadrada de un número impar y muestre el resultado con 3 dígitos. 
function calcularRaiz(){
    var num=parseInt(prompt("Ingrese el numero del cual desea sacar la raiz"));
    while(num % 2 == 0){
        var num = prompt("El numero ingresado no es impar, ingrese un nuevo numero: ")
    }
    var raiz= Math.sqrt(num);
    alert("Resultado de la raiz cuadrada: " +raiz.toPrecision(3));
}

// Ingresar una cadena de texto y mostrar la longitud de la cadena.
function longitudTexto(){
    var texto =prompt("Ingrese un mensaje ");
    alert("La longitud de su mensaje es de: " +texto.length);
}


// Mostrar la version del navegador
function versionNavegador(){
    alert("Informacion del navegador: " + navigator.userAgent);
}

// Mostrar el ancho y la altura de la pantalla.
function tamañoPantalla(){
    alert("Altura total de la pantalla  : "+screen.height+ "    Anchura total de la pantalla: "+screen.width);
}

// Imprimir la página.
function imprimir(){
    window.print();
}